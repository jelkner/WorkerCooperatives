# Note on Jeffrey Sachs Honest Interview About U.S. - Russia Nuclear Power

Available on YouTube at
[Jeffrey Sachs Honest Interview About U.S - Russia Nuclear Power and U.S Has No Plan At All?](https://www.youtube.com/watch?v=SOzVzFj__d0&t=2313).

* Sustainable development is an idea less than 40 years old. Dr. Bruntland's
  commission in 1987 brought the phrase to public awareness.

* The idea was adopted diplomatically at the Rio Summit in 1992 where it was
  formally inscribed as a goal of the member states of the United Nations.

* War is not inevitable and should be unthinkable.

* This idea of sustainable development is to say we choose to end all forms of
  human poverty, not all forms of human life.

* The *telos* of sustainable development is shared prosperity, environmental
  sustainability and global peace.

* These goals are within our reach, and to get there we should understand our
  stone age emotions what they make us prone to do and work to control the
  worst of them. We should understand and reform our medieval institutions
  because we don't want to die for medieval reasons.

* We should harness our technologies for the human good, not for war and
  destruction. The ultimate purpose of the digital revolution is not to have
  smarter guided missiles.
  
* The 17 sustainability goals were adopted in the Paris agreement on December
  12th, 2015 (*note*: and entered into force on November 4, 2016).

* This field of sustainable development is three things. First, it is a set of
  17 goals, making it a goal oriented discipline. Note that goal 16 calls for
  peaceful and inclusive societies and goal 17 calls for global partnership.

* 188 UN member states have put forward sustainable development goal plans of
  action. 5 UN member states have not: South Sudan, Yemen, Haiti, Myanmar,
  and the United States of America.
