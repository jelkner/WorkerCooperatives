# Eco Radicals - Notes and Resources by Participating Student

A repository of student work and course resources from a student participating
in **Eco Radicals: the Legacy of Revolutionary Cooperatives**


## Resources

* [Undergraduate Syllabus](UndergraduateSyllabus.md)
* [Google Doc Version of Undergraduate Syllabus](https://docs.google.com/document/d/1p_edXpU4pjzj2PEj4_ow1U-UBpjs7nDYl8BoZo6nDNA/view?tab=t.0)
* [Readings / Listenings](ReadingsListenings.md)
* [Graduate Syllabus](GraduateSyllabus.md)
