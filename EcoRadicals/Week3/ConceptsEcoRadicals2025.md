## 01/29/2025 Ravi [mail@ravisghosh.com](mailto:mail@ravisghosh.com)

## 02/05/2025 Alvin R. Elkerson

###  **Social Economy** Key Concepts in presentation:

  * Black Self-Determination
  * Economic Democracy
  * Cooperative Ecosystem
  * Solidarity Economy
  * Participatory Democracy
  * Worker-Owned Cooperatives
  * Community Land Trusts
  * Participatory Budgeting
  * Mutual Aid Networks
  * Green Economy
  * Democratic Governance
  * Cooperative Principles
  * Credit Unions
  * Community-Owned Utilities
  * Food Sovereignty
  * Cooperative Commonwealth
  * Separatist Black Cooperatives

### History, scholars, books

  * W.E.B. Du Bois, *Economic Cooperation* (1907)
  * The Rochdale Society of Equitable Pioneers (1844)
  * Karl Marx, *The Communist Manifesto* (1848)
  * Robert Owen, *A New View of Society* (1820)
  * Charles Fourier, *The Theory of the Four Movements* (1808)
  * Friedrich Wilhelm Raiffeisen, first rural credit union in Germany (1865)
  * Rosa Luxemburg, *The Accumulation of Capital* (1913)
  * Murray Bookchin, *The Ecology of Freedom* (1974)
  * Cooperation Jackson (founded 2014)
  * Chokwe Lumumba
  * Freedmen’s Bureau
  * The Knights of Labor (1880s)

## 02/12/2025 Anastasia Ryzhkova

## 02/19/2025 Elizabeth Plumeri

## 02/26/2025 Emily Richardson

## 03/05/2025 Jia Choi

## 03/19/2025 Jun Lee

## 03/26/2025 Makayla Hua-Ali

## 04/02/2025 Pujan Patel

## 04/09/2025 Ganesh Gopal

## 04/16/2025 Marie Therese Kane

## 04/23/2025 Soraya Barar

## 04/30/2025 Zeelie Moton


## Suggested:

* Social economy/Solidarity Economy
* Democratic Governance
* Electric Cooperatives
* Fair Trade
* Worker Coop
* Consumer Coop
* Social Coops
* Credit Unions
* Social profitability
* Decolonization
* Intersectionality
* Sustainable Development
* Food Sovereignty
* Self-determination
* Cultural Resistance
* Economic Democracy
* Separatism
* Municipalism
* Cooperative Ecosystem
* Federation of Cooperatives
* Cooperative Incubator
* Training Center
* Participatory Budgeting
* Green Economy
* ESOP
* Cooperism
* Cultural Hegemony
* Commons-based peer production
* Sociocracy
* Loomio (Tool)
* Historical Materialism
* Mode of Production
* Class Struggle
* Dialectical Change
* Base & Superstructure
* Democratic Employee Governance
* Egalitarian Wage Distribution
* Enhanced Intrinsic Motivation
* Innovation and Productivity Gains
* Mutual Aid
* Degrowth
* Community Wealth Building
* Sharing Economies
* Member-Managed Limited Liability Companies
* The Commons
* OpenDesk
* Namma Yatri
* Saccos
* Public Banking System
* White Revolution
* Kerala Food Platform
* Reparative Planning
* Bargaining for the Social Good
* Racial Capitalism
* Blues Epistemology
* Wake Labor
* The Undercommons
