# Meeting of May First Technology Movement on Thursday, 

"The Internet doesn’t just happen to us. We have a relationship with the
Internet, and we have the ability to change our relationship to the Internet to
advance liberatory movements." ~ Jamie McClelland


## Links

* [Etherpad Notes from meeting Riseup](https://pad.riseup.net/p/engagement)
