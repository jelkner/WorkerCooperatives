# Webinar: Impact of Artificial Intelligence on Social Movements

A webinar with [Paola Ricaurte
Quijano](https://cyber.harvard.edu/people/paola-ricaurte-quijano) hosted by
[MayFirst](https://mayfirst.coop/en/) membership coordinator
Jes Ciacci.  Jes is co-founder of
[Sursiendo](https://sursiendo.org/).

The presentation was given in Spanish, but I was amazed by how effective the
simultaneous translation into English worked on the
[Jitsi](https://meet.jit.si/) virtual meeting. There were about 85 people who
attended the meeting, and the tech worked very smoothly.

I've already been reading a lot about
[LLM's](https://en.wikipedia.org/wiki/Large_language_model), and much of the
discussion centered around what "AI" is and how it will impact us. The
presenter made the point that AI can be very useful.
