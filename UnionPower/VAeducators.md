# Virginia Education Union

## Collective Bargaining in Fairfax County Public Schools

* [27,000 Virginia education workers win union recognition](https://www.peoplesworld.org/article/27000-virginia-education-workers-win-union-recognition/)
* [Big Union Win in Virginia Schools where Bargaining Suddenly
Legal](https://www.labornotes.org/2024/06/big-union-win-virginia-schools-where-bargaining-suddenly-legal)
* [Over 27,000 K-12 Educators Just Unionized in
Virginia](https://jacobin.com/2024/06/fairfax-county-teachers-unionization)
* [Fairfax County Public Schools workers vote to officially
unionize](https://www.ffxnow.com/2024/06/11/fairfax-county-public-schools-workers-vote-to-officially-unionize/)
* [Fairfax Education Unions -
About](https://www.fairfaxeducationunions.org/about)


## NEA, AFT Merged Unions

* [More Mergers for NEA, AFT
Affiliates](https://www.edweek.org/teaching-learning/more-mergers-for-nea-aft-affiliates/2013/02)
* [Union Report: Will NEA and AFT Ever Merge? They Last Tried in 1998. Things
Are Much Different
Now](https://www.the74million.org/article/union-report-will-nea-and-aft-ever-merge-they-last-tried-in-1998-things-are-much-different-now/)


## General Education Union

* [National Teachers Unions Look to the
South](https://americansforfairtreatment.org/2021/10/07/national-teachers-unions-look-to-the-south/)
