I am an active member of my local union, the Arlington Education Association
(AEA) because I believe deeply that strong, powerful unions are the only hope
we have to move toward the promise of building an equitable, democratic society
in the United States.  As union participation rates have continued to decline
over the last almost half century, inequality has been steadily rising.  The
last time working folks got a raise in the U.S. was around the year I graduated
high school, 1979, and I have witnessed throughout my adult life the loss of
power and worsening conditions of the working people in my country.

As a parent of two wonderful boys around the age of 30, I am frustrated that my
generation propped up and participated in an economic system that puts profits
before people and planet, and has as a result put the latter two in great peril
for the future.  My boys are part of the first generation that will be poorer
and live more precarious lives than their parents did.

Only those of us who work for a living are in a position to understand and
change this deteriorating situation, and we can only do that through the
collective power of our union.
