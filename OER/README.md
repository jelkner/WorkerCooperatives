# OER Computer Science

## Motivation

[Arthur C. Clarke's third
law](https://en.wikipedia.org/wiki/Clarke%27s_three_laws) states that "any
sufficiently advanced technology is indistinguisable from magic."

In Raspberry Pi founder Eben Upton's
[2013 Keynote at Pycon](https://pyvideo.org/pycon-us-2013/keynote-2.html),
he describes both the declining number and skills of the computer science
applicants to the Cambridge University CS program by 2005 relative to a decade
earlier, which he attributes to the lack of "hackable" machines available to
young learners compared to those available to his generation in the 1980s.

In my 30+ years teaching computer programming to high school students, I have
witnessed the phenomenon Upton describes first hand, and only seen it get
worse. The computers we issue to students at my school in our
[one-to-one](https://en.wikipedia.org/wiki/One-to-one_computing) these days
are "locked down", preventing students from learning how to control the device,
and the slick, modern operating system through which they interact with the
machine is becoming ever more indistinguishable from magic.

For aspiring computer scientists and technologies at the very least, but I
would argue for anyone who wishes to possess *21st century literacy*, this is
not a good situation.  If students do not understand the machines which have
ever increasing impact on their lives, how are they going to reason about them?


## Vision

Given the above movitation, I hope to develop a curriculum for my students
with the central goal of *demystifying the computer*. I want students to leave
our program with a deep understanding of what a computer is, what it can and
cannot do, and what the implications of its use are for the world we humans are
building in the 21st century.


## Opportunity

I am fortunate indeed to be in just the right place to work toward the above
vision.  I teach in a small
[dual-enrolled](https://en.wikipedia.org/wiki/Dual_enrollment) program that
offers an associate degree in computer science concurrent with a high school
diploma.

That means am one of only two instructors who work with cohorts of students
over four years through the following sequence of courses:

- [Advanced Placement Computer Science Principles](https://en.wikipedia.org/wiki/AP_Computer_Science_Principles)
- [Introduction to Problem Solving and Programming](https://courses.vccs.edu/courses/CSC221-IntroductiontoProblemSolvingandProgramming)
- [Object-Oriented Programming](https://courses.vccs.edu/courses/CSC222-ObjectOrientedProgramming)
- [Introduction to Discrete Structures](https://courses.vccs.edu/courses/CSC208-IntroductiontoDiscreteStructures)
- [Computer Systems](https://courses.vccs.edu/courses/CSC215-ComputerSystems)
- [Data Structures and Analysis of Algorithms](https://courses.vccs.edu/courses/CSC223-DataStructuresandAnalysisofAlgorithms)

Because we have a small school program with student cohorts, we are in a
unique position to be able to design an academic program that is integrated
over time, with our overall learning objectives effectively distributed
throughout our computer science pathway.


## Goal

I want to develop integrated open educational resources for each of the
courses in our computer science pathway.  By both adapting existing OER
materials and creating our own, we can develop learning materials that are
high quality, equitably available, and customed tailored to meet the needs of
our program.


## Resources

Here are the courses and a work in progress list of available resources:

### Advanced Placement Computer Science Principles

* [CS Principles: Big Ideas in Programming](https://www.openbookproject.net/books/StudentCSP/)

### Introduction to Problem Solving and Programming

* [Python for Everybody](https://www.py4e.com/) by Charles Severance
  (aka "Dr. Chuck")

### Object-Oriented Programming

* [How to Think Like a Computer Scientist: Learning with C++](http://www.openbookproject.net/thinkcs/cpp/)
  by Allen B. Downey, Paul Bui, and Jeffrey Elkner

### Introduction to Discrete Structures

* [Discrete Mathematics: An Open Introduction 3rd Edition](https://discrete.openmathbooks.org/dmoi3.html)
  by Oscar Levin

### Computer Systems

* [Altair 8800 Operator's Manual](https://ubuntourist.codeberg.page/Altair-8800/)

### Data Structures and Analysis of Algorithms

* [How to Think Like a Computer Scientist: Learning with C++](http://www.openbookproject.net/thinkcs/cpp/)
  by Allen B. Downey, Paul Bui, and Jeffrey Elkner
