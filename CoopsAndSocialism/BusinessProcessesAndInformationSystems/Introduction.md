# Introduction

Our goal here will be to focus on the concept of business processes and the
ability for information systems to enhance or automate business processes.

We should begin by examining the concept of a business process. A business
process is defined as a series of tasks that are completed in order to
accomplish a goal. Henry Ford achieved international fame by taking advantage
of the assembly line in which a series of small tasks were each completed by an
individual in the "assembly line" in order to accomplish the goal of
manufacturing the Model T automobile. This series of steps to manufacture a
vehicle is a manufacturing business process.  In every organization, there are
a number of processes that must be executed in order for the organization to
achieve its goals and create value for its consumers. Sales, finance,
manufacturing, human resources, accounting, payables, receivables, payroll,
reporting all incorporate business processes.  The characteristic of a business
process is that it has defined repeatable steps that must be executed in order
to accomplish a goal, in many cases the process lends itself to either being
automated or enhanced with information systems. 

We will explore categories of common software solutions that are aligned with
specific types of business processes. For example, ERP or Enterprise Resource
Planning solutions are aligned with a variety of business processes. CRM or
Customer relationship management solutions align with processes focused on
selling and engaging with customers. PLM or Product Lifecycle Management
solutions are aligned with product development and engineering processes. 

Another area that we will explore is the concept of business process
reengineering. Business processes are developed to accomplish a specific goal.
In many organizations, these business processes are long-lived and the specific
tasks required to accomplish the goal change over time as technology innovation
presents new opportunities to execute the process more efficiently. For
example, in the past, the payroll process would have included computing the
wages and deductions for each employee and then printing and distributing
payroll checks. Technology has changed this process in most organizations such
that funds are distributed to employees eliminating the need to produce,
distribute, and process checks. Eliminating these steps from the process of
payroll reduces costs and provides added value to the employees. Eliminating
the steps of printing, distributing, and processing checks is a reengineering
of the process. In many cases, organizations opt to evaluate and reengineer
business processes as part of the implementation of new software solutions.
Such projects have experienced mixed results. 

The systematic review of the tasks required to achieve a goal is referred to
as processes modeling and is implemented as part of process engineering or
reengineering efforts.
