# Supporting Business Processes Through Information Systems

## Topics

* Defining the Business Processes
* Automating Business Processes
    * Enterprise Resource Planning
    * Supply Chain Management
    * Customer Relationship Management
    * Product Lifecycle Management
* Business Processing Modeling
* Business Process Management
* Business Process Re-engineering

## Learning Objectives

* Describe and define business processes
* Explain the value of an enterprise resource planning (ERP) system
* Explain how business process management and business process rengineering
  work 
* Examine how information technology combined with business processes can bring
  an organization competitive advantage
