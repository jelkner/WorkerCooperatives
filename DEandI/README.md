# Diversity, Equity &amp; Inclusion - PD Reflections

I work as a teacher for [Arlington Public
Schools](https://en.wikipedia.org/wiki/Arlington_Public_Schools).  As part
of our institution wide [professional
development](https://en.wikipedia.org/wiki/Professional_development) this year,
we are taking a course introducing [diversity, equity, and 
inclusion](https://en.wikipedia.org/wiki/Diversity,_equity,_and_inclusion).
This document will contain my journal for that course.


## Reflection 1 - What is diversity, equity, and inclusion?

Diversity, equity, and inclusion is a framework of theory and practice
aimed at creating a welcoming, equitable environment for all of the diverse
human beings who make up our human community.


## Diversity Definition

At APS, diversity uses the intentionally more inclusive **DARNSCARS framework**
(disability, age, race, national origin, sex, color, armed services status,
religion, sexual orientation) rather than the more narrow **BIPOC** (Black,
Indigenous, and people of color), and illustrates our efforts to increase our
exposure to employees, students, families, and communities from the broad range
of our human family. 


## Equity Definition

Equity is about enhancing opportunities and mitigating risks for everyone,
in accordance with their varying needs.


## Inclusion Definition

Inclusion is about creating an enviroment that supports and encourages
*humanization* for all members of the group or community.


## Reflection 2 - Why should we (APS) focus on DE&amp;I?

As a self-described [Freiren](https://en.wiktionary.org/wiki/Freirean), I
believe that humanization is both our ontological and historical vocation.
This is true for all people, but none more than educators. Since DE&amp;I
is in support of this vocation, we should focus on it.


## Reflection 3 - Shift in Available Talent Trends

> We are asked here to discuss how we think the demographic trends within
> the U.S. workforce, the changing racial profile in successive generations,
> and the increasing numbers of educators either leaving or expressing a desire
> to leave the profession will impact the success of our colleagues, students,
> and communities. 

If our schools fail to effectively serve the increasingly diverse students,
which requires attracting and retaining educators to do this, the very future
of our nation, economically and culturally, will be lost. Given the stark
nature of this reality, the question becomes how to create a school
environment that can truly serve our students, attract and retain staff,
and help us prepare for our shared future. 

While I often tell anyone who will listen that "I have the best job in the
world", I am fully aware of the many reasons that feeling is not more
widespread among my colleagues. In my role as a dual-enrolled computer science
teacher, I have a great deal more academic freedom than do most of my peers.

Since my classes are elective, I do not face nearly as many challenges with
student behavior, since the students who do not want to be in my classes don't
take them.

As a long time veteran, I make a point of taking extra planning time instead of
doing many of the silly, useless, demotivational things we are often asked to
do. And I have found increasing opportunities to collaborate with both students
and colleagues that is adding to the joy I find in coming to work at school.


## Reflection 4 - Benefits of a DE&amp;I Focus 

> We were tasked in this reflection to identify three benefits of a DE&amp;I
> focus.

Among the many benefits of a DE
