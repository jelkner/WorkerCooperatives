# Notes on [Jackson Rising](https://jacksonrising.pressbooks.com/)

## Quotes

* "[Vision] for a more just society -- a society that is rooted in
  shared governance, collective stewardship and use of resources, and unity in
  diversity." ~ *Forward: All Roads Lead to Jackson*, Rukia Lumumba
* "Cooperation Jackson is a vehicle specifically created to advance a key
  component of the Jackson-Kush Plan, namely the development of the solidarity
  economy in Jackson, Mississippi, to advance the struggle for economic
  democracy as a prelude towards the democratic transition to eco-socialism."
  *1: Build and Fight: The Program and Strategy of Cooperation Jackson*,
  Kali Akuno
* "A population or people that does not have access to and control over these
  means and processes cannot be said to possess or exercise
  self-determination." *1: Build and Fight: The Program and Strategy of
  Cooperation Jackson*, Kali Akuno
* [W]e are positioning ourselves to act as a “developer”, which is normally a
  role that is exclusively played by the bourgeoisie, i.e. the capitalist
  class, or the state. We are aiming to upend this paradigm on many levels and
  in several strategic ways. *1: Build and Fight: The Program and Strategy of
  Cooperation Jackson*, Kali Akuno
* "In order to democratically transform the capitalist world-economy, we have
  to transform the agent central to this process, the working class, into a
  democratic subject." *1: Build and Fight: The Program and Strategy of
  Cooperation Jackson*, Kali Akuno
* "[S]elf-determination is unattainable without an economic base. And not just
  your standard economic base, meaning a capitalist oriented one, but a
  democratic one." *1: Build and Fight: The Program and Strategy of
  Cooperation Jackson*, Kali Akuno
* "Self-determination is not possible within the capitalist social framework,
  because the endless pursuit of profits that drives this system only empowers
  private ownership and the individual appropriation of wealth by design. The
  end result of this system is massive inequality and inequity. We know this
  from the brutality of our present experience and the nightmares of history
  demonstrated to us time and time again over the course of the last 500
  years." *1: Build and Fight: The Program and Strategy of Cooperation
  Jackson*, Kali Akuno
