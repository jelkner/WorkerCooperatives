# Caddy Installation Notes

## Links

* [Caddy Installation on Debian, Ubuntu, Raspian](https://caddyserver.com/docs/install#debian-ubuntu-raspbian) Used Stable releases instructions
* [Caddy equivalent to apache2's 'mod\_userdir' module](https://caddy.community/t/caddy-equivalent-to-apache2s-mod-userdir-module/3189)
