# Misión de SECOSOL

Fortalecer las capacidades de las comunidades Latinas inmigrantes para crear
proyectos de economias solidarias incluyentes a traves de la promocion,
educacion e incidencia.


# Plan estratégico de SECOSOL para 2024


## Objetivo 1

Robustecer la capacidad administrativa, operativa y financiera de SECOSOL a
través de la busqueda de $50K para finales de Diciembre 2024.

### Actividades:

1. Contratar escritor de subvenciones.
2. Identificar los recursos adentro de la junta para apoyar Averie Fitzsimons
   (interna).
3. Crear un documento para la comunicacion y discusion para el plan estratigico.


## Objetivo 2

Desarrollar las capacidades administrativas, técnicas y financieras  de la
Cooperativa Mujeres Manos a la obra para lograr su autonomía para finales de
Diciembre de 2024.

### Actividades:

1. Consultar con colmenar cooperative consulting (Daniella Priesler).
2. Justicia sanadora con Leslie Moncada.
3. Ayuda a Mujeres Manos a la Obra a unirse a la Federación de Cooperativas de
   Trabajadores de los Estados Unidos.


## Objetivo 3

Sistematizar y recuperar las experiencias y aprendizajes de dos cooperativas de
mujeres en el sector de limpieza, para visibilizar las problemáticas que
enfrentaron y  recuperar su sabiduría sobre los procesos de construcción
operacional con enfoque feminista para finales de 2024.

### Actividades:

1. Crear cronograma.
2. Presentar la propuesta de sistematizacion a las co-ops para su aprobacion.
3. Desarrollar grupos focales.
4. Entrevistas a informantes clave.
5. Acompanamiento a las reuniones mensuales de las co-ops.
6. Observación de sus procdesos de negociación y ejecucion de su trabajo.
7. Elaboración de doc con resultados.
8. Presentación y retroalimentacion de los resultados.
9. Elaboracion de reporte final.


## Objetivo 4

Crear directorio de organizaciones en la economía feminista y solidaria para
desarollar la capacidad de las cooperativas suscritas a SECOSOL (Mujeres Manos
a la obra, Nova web development, Escoba mágica, y Latinas en Poder), para fines
de Diciembre 2024.

### Actividades:

1. Identificación de las organizaciones del sector.
2. Crear un documento que contenga información básica sobre las organizaciones:
   * Nombre
   * Telefóno
   * Páginas digitales
   * Quiénes son, Misión y Capacidades de la organización, lugar de
     operación
3. Crear espacios para la comunicación con las organizaciones y posible
   acuerdo de entendimiento.


## Objetivo 5

Identificar desde la sitematización de las cooperativas áreas temáticas que
orienten el desarrollo de un curriculum de 7 módulos para finales de Diciembre
del 2024.

### Actividades:

1. Taller participativo para la determinación de los temas del curriculum.
2. Conceptualización de los temas basados en la sistematización y los talleres
   de retroalimentación.
3. Elaboracion del documento final con los ejes temáticos para el diseño del
   curriculum.


## Objetivo 6

Realizar campaña de incidencia política para cambiar las percepciones sobre el
movimiento cooperativo de mujeres para finales de Diciembre, 2024.

### Actividades:

1. Crear las líneas de la campaña.
2. Identificación -Mapa de Poder.
3. Crear lazos estratégicos con organizaciones, cooperativas y sociales y
   legislativos.
4. Crear contenido y líneas creativas de la campaña, materiales de educación y
   concienciacion, y otros.
5. Lanzamiento de la campana en redes sociales y medios de comunicacion.
6. Finalización de la campana:
   * Comunicado y Documento sobre proceso y resultados de la campaña.
   * Identificar próximos pasos.


