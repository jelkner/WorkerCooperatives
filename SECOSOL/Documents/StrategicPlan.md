# Mission of SECOSOL

Strengthen the capacities of Latina immigrant communities to create inclusive
solidarity economy projects through promotion, education and advocacy.


# Strategic Plan of SECOSOL for 2024


## Objective 1

Strengthen the administrative, operational and financial capacity of SECOSOL
by increasing funding for the organization to $50K by the end of December 2024.


### Activities:

1. Hire grant writer.
2. Identify resources within the board to support Averie Fitzsimons (internal).
3. Create a document for communication and discussion for the strategic plan.


## Objective 2

Develop the administrative, technical and financial capabilities of the
Mujeres Manos a la Obra cooperative to achieve its autonomy by the end of
December 2024.

### Activities:

1. Consult with Colmenar Cooperative Consulting (Daniella Priesler).
2. Provide healing justice training with Leslie Moncada.
3. Help Mujeres Manos a la Obra join the US Federation of Worker Cooperatives.


## Objective 3

Systematize and recover by the end of 2024 the experiences and learning of two
cooperatives of women in the cleaning sector to make visible the problems they
face and recover their wisdom about the construction processes operational
within a feminist focus.

### Activities:

1. Create a schedule.
2. Present the systematization proposal to the co-ops for approval.
3. Develop focus groups.
4. Interview key informants.
5. Accompany them at monthly co-op meetings.
6. Observe their negotiation processes and execution of their work.
7. Prepare a document with results.
8. Present results and get feedback.
9. Prepare the final report.


## Objective 4

Create a directory of organizations in the feminist and solidarity economy
movements for use by member cooperatives of SECOSOL (Mujeres Manos a la Obra,
NOVA Web Development, Magic Broom, and Latinas en Poder) to develop their 
capacity as worker cooperatives by December 2024.

### Activities:

1. Identify organizations in the sector.
2. Create a document containing basic information about the organizations:
    * Name
    * Phone
    * Digital pages
    * Who they are, their mission and capabilities, and location of operation
3. Create spaces for communication with these organizations and possible
   agreements of understanding with them.


## Objective 5

Identify from the systematization of cooperatives thematic areas that guide the
development of a 7-module curriculum by the end of December 2024.

### Activities:

1. Conduct a participatory workshop to determine curriculum topics.
2. Conceptualize topics based on systematization and workshop feedback.
3. Preparae a final document with thematic axes for the design of a curriculum.


## Objective 6

Carry out a political advocacy campaign to change perceptions about the women's
cooperative movement by the end of December, 2024.

### Activities:

1. Create campaign plan.
2. Identify Power Map.
3. Create strategic ties with organizations, cooperatives and social and
   legislative organizations.
4. Create content and creative development plan for the campaign, and education
   and awareness materials.
5. Launch of the campaign on social networks and media.
6. End the campaign:
   * Release a communiqué and document on the process and results of the
     campaign.
   * Identify next steps.
