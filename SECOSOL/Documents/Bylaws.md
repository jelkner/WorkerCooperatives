# BYLAWS OF SECOSOL


## Article I: Name, Purposes, Values and Offices

### Section I.1 Name.

The name of the corporation is SECOSOL. (the “Corporation”).

### Section I.2 Purposes and Powers.

The Corporation engages in building movements by advancing the strategic use
and collective control of technology for local struggles, global transformation
and emancipation without borders.


### Section I.3 Values.

The Corporation operates according to the following values:

* cooperation, collaboration and sharing help us contribute to a movement
  broader than itself;
* transparency, openness and honesty ensure that it is building a democratic
  organization;
* equality and respect guide it in building with the diversity of people it
  needs to make a difference;
* conviction and discipline provide the strength to continue against
  overwhelming odds;
* commitment to fighting racism, sexism and exploitation;
* solidarity, mutual aid and sustainability ensure it is building new ways to
  work together; and
* humility, openness to criticism and commitment to conflict resolution create
  a culture of resilience for the Corporation and movements.

## Article II: Membership

### Section II.1 Membership.

The Corporation shall have three classes of members:

1. organizational,
2. individual and
3. worker.

All members must pay annual dues of $*dues amount* USD (or the equivalent if
paying in a different currency, based on a rate set annually by the Board).

A. Organizational members are members that join the Corporation on behalf of a
   worker cooperative. Their primary use of the Corporation’s services is in
   support of their cooperative.
B. Individual members are members who join the Corporation on their own behalf,
   whose primary use of the Corporation’s services is personal.
C. Workers are members, including both paid staff and volunteers of the
   Corporation, that provide labor to the Corporation as is directed by the
   board of directors of the Corporation (the “Board”). Paid staff shall earn a
   salary reflecting social justice movement standards as defined by the Board.
D. Members may pay dues to a local organization instead of directly to the
   Corporation, provided such local organization makes a commitment to engage
   in movement building consistent with the mission of the Corporation and has
   a special relationship with the Corporation as may be evidenced by
   (i) the approval of the relationship by a majority of the membership and,
   (ii) following such approval, an agreement between the Corporation and such
   organization as shall be negotiated by the Board.
E. Membership shall be open to anyone interested in the mission of the
   Corporation, regardless of such person’s country of residence. The Board
   may establish additional fees for additional membership benefits and such
   fees may vary based on the member’s access to financial resources, as the
   Board deems appropriate.

### Section II.2 Meetings.

There shall be an annual meeting of the members (the “Annual Meeting of the
Members”) for the election of the directors, the review of a financial report
and the transaction of other business. Additional special meetings of the
members shall be held whenever called by resolution of the Board or by a
written demand to the Board of ten percent (10%) of the members eligible to
vote, such demand that may be made through an online mechanism that the Board
shall put in place.

### Section II.3 Notice of Meetings.

Written notice stating the place, date, time and purpose (in the case of a
special meeting) of any member meeting shall be posted on the Corporations’
website and sent via email or similar delivery method to each member entitled
to vote at such meeting.  Notice shall be given not less than ten (10) days
before the date of such meeting.

### Section II.4 Quorum and Adjournment of Meetings.

At all meetings of the members, the presence, in person or by proxy, of ten
percent (10%) of the members eligible to vote or one hundred (100) members
eligible to vote shall constitute a quorum for the transaction of business. In
the absence of a quorum, the members present may adjourn the meeting.

### Section II.5 Organization of Meetings.

The Board will choose a chair and secretary at all meetings of the members.


### Section II.6 Voting.

At any meeting of the members, each member present, in person or via electronic
communication, shall be entitled to one (1) vote, except as provided in Section
III.4 below. Upon demand of any member, any vote for directors or upon any
question before the meeting shall be by ballot. The record date for determining
eligibility to vote shall be not less than ten (10) nor more than fifty (50)
days before the date of the meeting. Once a vote is called, the voting period
for members to vote via electronic communication shall expire after seven (7)
days.

### Section II.7 Action by the Members.

Except as otherwise provided by statute or by these bylaws of the Corporation
(these “Bylaws”), any corporate action authorized by a majority of the votes
cast at a meeting of members shall be the act of the members. Action may be
taken without a meeting if all members consent to the adoption of a resolution
authorizing the action.  Such consent shall be set forth the action so taken
and shall be submitted in writing or via email or facsimile. The resolution and
consents thereto shall be filed with the minutes of the proceedings of the
membership.

### Section II.8 Special Actions Requiring Vote of Members.

The following corporate actions may not be taken without approval of the
members:

1. a plurality of the votes cast at a meeting of the members is required for
   the election of directors of the Corporation;
2. a majority of the votes cast at a meeting of the members is required for
   any amendment of or change to the Certificate of Incorporation or these
   Bylaws;
3. two-thirds of the votes cast at a meeting of the members is required
   for:
     (1) a sale, lease, exchange or other disposition of all or substantially
     all of the assets of the Corporation;
     (2) a plan of merger, consolidation or dissolution of the Corporation; or
     (3) revocation of a voluntary dissolution proceeding;
   provided, however, that the affirmative votes cast in favor of any action
   described in this subsection (C) shall be at least equal to the minimum
   number of votes necessary to constitute a quorum. Blank votes or
   abstentions shall not be counted in the number of votes cast.


## Article III: Board of Directors

### Section III.1 Power of the Board.

The Board shall be responsible for (i) managing the business, property, affairs
and activities of the Corporation, (ii) conducting a democratic, participatory
membership meeting process that generates input from the membership and (iii)
providing strategic direction for the Corporation.

### Section III.2 Qualifications of Directors.

Each director shall be at least eighteen (18) years old. Each director shall be
at least eighteen (18) years old. Members who pay dues to a local organization
in accordance with Section 2.1 above shall have a proportion of the at-large
seats on the Board that is equal to the proportion of their organization's
membership relative to the total membership of the Corporation.

### Section III.3 Number of Directors.

The Board shall consist of a minimum of twenty-one (21) and a maximum of
twenty-five (25) directors. Within these specified limits, the number of
directors may be increased or decreased by action of the Board or the members,
provided that no decrease shall shorten the term of any incumbent director. The
term “Entire Board” means the number of directors that were elected as of the
most recently held election of directors, as well as any directors whose terms
have not yet expired.

### Section III.4 Election and Term of Directors.

Eighty percent (80%) of the directors shall be elected at-large by all eligible
members of the Corporation with individual members being entitled to one (1)
vote per individual and organizational members being entitled to two (2) votes
per organization. Twenty percent (20%) of the directors shall be elected by
worker members of the Corporation only. The at-large directors will be elected
to three-year terms. In the event that there is not an even distribution of
directors serving one-, two- and three year terms, the terms for at-large
directors may be modified prior to such election such that the member winning
the highest number of votes receives the longer terms, followed by the member
winning the second highest number of votes receiving the second longest term
and so forth until all of the members winning enough votes for election as
directors are installed. Directors elected by worker members will have one-year
terms. At each Annual Meeting of the Members, persons shall be nominated and
elected by the members to replace those directors whose terms are expiring,
each director thereafter to serve until their successor is elected. If the
number of directors is changed by the Board or the members in accordance with
these Bylaws, any increase or decrease shall be apportioned among the classes
of directors in order to maintain the number of directors in each class as
nearly equal as possible.

### Section III.5 Vacancies and Newly-Created Directorships.

Vacancies occurring in the Board for any reason and newly-created directorships
resulting from an increase in the authorized number of directors shall be
filled at a meeting of the members by a plurality of the votes cast.  Each
director so elected shall serve until the next Annual Meeting of the Members at
which the election of directors is the regular order of business and until
their successor is elected.

### Section III.6 Resignation.

Any director may resign at any time by delivering notice to the Chair or the
Secretary in writing or by e-mail. The resignation shall take effect when such
notice is so delivered, unless the notice specifies a later effective date, and
the acceptance of such resignation shall not be necessary to make it effective.

### Section III.7 Removal.

Any one or more of the directors may be removed for cause or without cause at
any time by the affirmative vote of two-thirds of the members present, in
person or by proxy, at a regular meeting or special meeting of the members
called for that purpose; provided that there is a quorum present at such
meeting and that notice of the proposed action shall have been transmitted to
all members in accordance with Section II.3 of these Bylaws, at least ten
days before said meeting. Any one or more of the directors may be removed for
cause at any time by the affirmative vote of two-thirds of the directors
present at a regular meeting or special meeting of the Board called for that
purpose; provided that there is a quorum of not less than a majority of the
Entire Board present at such meeting and that notice of the proposed action
shall have been transmitted to all directors at least one week before said
meeting. Director may be removed by the Board for egregious behavior, including
but not limited to theft of resources, behavior that violates the
organization’s values or failure to attend three Board meetings without
notifying the Board of that absence before the meeting.

### Section III.8 Meetings.

The Board shall meet quarterly for the transaction of such other business as
may properly come before the meeting. Regular meetings of the Board shall be
held at such times and places as may be fixed by the Board. Special meetings of
the Board may be held at any time upon the call of the Chair or by any director
upon written demand of at least one-fifth of the Entire Board, at the time and
place fixed by the person or persons calling the special meeting.

### Section III.9 Notice of Meetings.

Notice need not be given of regular meetings of the Board if such meetings are
fixed by the Board. Notice shall be given of each regular meeting not fixed by
the Board, and each special meeting of the Board. Notice shall be sent via
e-mail to each director, at their e-mail address as it appears in the records
of the Corporation, at least two (2) calendar days before the day of the
meeting. Notice shall include the date, time, and place of the meeting, and,
for each annual and special meeting, shall be accompanied by a written
agenda setting forth all matters upon which action is proposed to be taken.

### Section III.10 Quorum.

Unless a greater proportion is required by law or by the Certificate of
Incorporation or these Bylaws, at each meeting of the Board, a majority of the
Entire Board shall constitute a quorum for the transaction of business. If a
quorum is not present at any meeting of the Board, a majority of those
directors present may adjourn the meeting until such a quorum is present.
Directors who are present at a meeting but not present at the time of a vote
due to a conflict of interest or related party transaction shall be determined
to be present at the time of the vote for purposes of this paragraph.

### Section III.11 Participation in Meetings via Technology.

Any one or more directors may participate in any meeting of the Board or any
committee thereof by means of a conference telephone, video conference, or
similar communications equipment allowing all persons participating in the
meeting to hear and speak to each other. Participation by such means shall
constitute presence in person at a meeting for all purposes, including quorum
and voting.

### Section III.12 Action without a Meeting.

Any action required or permitted to be taken by the Board or any committee of
the Board may be taken without a meeting if it is proposed via email, provides
a minimum of five (5) days for the Board to respond, and is unanimously
approved by Board members who participate in the vote (assuming there is
quorum). The resolution and consents thereto shall be filed with the minutes of
the proceedings of the Board or committee.

### Section III.13 Organization of Meetings.

The Board shall assign a member of the Board to record the minutes of each
meeting and shall retain such minutes with the permanent records of the
Corporation.

### Section III.14 Compensation of Directors.

The Corporation shall not pay any compensation to directors for their services
as directors of the Corporation, except that directors may be reimbursed for
reasonable and necessary expenses incurred in the performance of their duties
to the Corporation. Subject to the Corporation’s conflict of interest policy
and applicable law, directors may receive reasonable compensation for services
performed in other capacities for or on behalf of the Corporation pursuant to
authorization by the Board.


## Article IV: Committees

### Section IV.1 Committees of the Board.

The Board may create committees of the Board, each consisting of three or more
directors. At least one of the co-chairs of each committee shall be a woman
and/or person of color. The Board shall appoint the members of any such
committee of the Board by a majority of the Entire Board.

### Section IV.2 Worker Review Committee.

Each year, the Board shall create a worker review committee (“Worker Review
Committee”), which may formally hire and fire workers, both paid and volunteer,
and conduct annual worker evaluations. The Worker Review Committee must have
five (5) members, including two (2) selected by a majority of the workers and
at least four (4) members who are women or people of color.


## Article V: Amendments to Bylaws

Subject to the notice requirements of Section II.3, these Bylaws may be
adopted, amended or repealed in whole or in part at any meeting of the members,
if a quorum is present at the time of the vote, by the affirmative vote of a
majority of the votes cast.

Adopted *date adopted* 
