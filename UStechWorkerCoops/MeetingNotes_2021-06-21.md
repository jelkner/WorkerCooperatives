# Meeting Notes June 21, 2021

In attendence: Nurul, Colombene, Jeff, Steve

## Proposed Agenda Items for 6/21 Meeting:
    
1. Date(s) for webinar with Yael Richardi.
    1. Maximize attendance
    1. Share with USFWC tech coop group?
    1. start w/ Show and Tell Group- do at a Show and Tell? 3p+ Central
       Time ish.  Mid/ late week of  07/28 -31. Propose w/ Show and Tell
       (2p CT) 07/29.
    1. An hour to begin with
        1. Agenda - Why we're here
        1. Presentation by Yael / FACTIC in Argentina - Nicolas DiMarco of Fiqus
            1. see also Coop.tech
            1. see [awesomeopensource](https://awesomeopensource.com/project/hng/tech-coops?fireglass_rsn=true#coops)
            1. Q&A
            1. Next Steps/ Timing
            1. Once date and time set, we can set up an RSVP page/ eFlyers, etc
1.  Update on / plans for Decidim Summer Sprint

[Summer Sprint Goals](https://codeberg.org/jelkner/SolidarityEconomyNotes/src/branch/main/Decidim/SummerSprintGoals2021.md)
