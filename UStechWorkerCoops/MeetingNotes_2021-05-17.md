# US Tech Worker Coop Meeting Notes 2021-05-17

## Present: Colombene (CA), Nurul (IL), Jeff (VA)

## Resources

* Jeff can devote July 6 - 30 to dedicated Decidim work,
  along w/ 5-6 interns to *Decidim Summer Sprint*.
* [Talk on big tech in municipal governance](https://www.domusweb.it/en/events/forum-2018/2018/evgeny-morozov-cities-have-become-a-magnet-for-big-technology-investments-and-platforms.html)
* [Tech coops list](https://github.com/hng/tech-coops)


## Objective: Minimum Viable Community (Canvas!?)

* People 
* Norms/ rules
* Place (physical or virtual)    
* Outcomes
* Interdependence
* Trust/ Mutual Accountabilities
* Tools

Model a non-exploitative cooperative business ecosystem.

## Community Models:

* [A very big list of community models and ideas ](https://rosie.land/posts/a-very-big-list-of-community-models-and-ideas)
* [US Tech Worker Coops Proposals](https://ustechworkercoops.org/processes/tech-worker-coop-creation/f/10/)

Colombene browsed [documentation](https://docs.decidim.org/en/) and saw that
Admin docs were more useful than feature docs.

Q: How do other countries (ARG) mutually refer business to each other?

Q: How do we assess health of a tech co-op ecosystem?

* existence of one in the first place
* number of actors (orgs, workers, clients)
* financial health of actors (living wage+)
* longevity of actors 
* number and quality of business referrals
* revenue$ generated
* variety of ecosystem

Q:  How does community relate to USTWC directory that has started?

## Needs:

* Infrastructure
* Community bldg.
* Mutual referrals/ sales
* Business process
* Understandable documentation
* Friendly on-ramps for newcomers
* Potential ERP?
* Avoid toxicity and poor behavior
* Avoid monopolies and monoculture

Building a US Tech Worker CoOp community a'la UK CoTech, Argentina FAC[TIC]

Assumption: These are healthy and sustainable ecosystems, at least in part,
and worth emulating.


## Homework

* Consolidate notes (Jeff volunteering to do that), on Codeberg.
* Nurul can schedule meeting
* Further consider the characteristics of a minimal viable community.
* Reach out to Camba about a possible "nuts-and-bolts" webinar on forming a
  tech worker cooperative federation.
* Brainstorm potential "anchor" clients for Decidim instance and/or Minimum
  Viable Community
* Beware of too tightly coupling Decidim Tool and Community (but balance)
