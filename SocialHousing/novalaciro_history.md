# Brief History of NOVALACIRO

The organizatin that would become NOVALACIRO was founded in 2005 by the late
community activist [Charlie
Rinker](https://countyboard.arlingtonva.us/tribute-day-charlie-rinker/) as
BRAVO, Buyers and Renters Arlington Voice, a grass roots organization dedicated
to housing justice. Charlie served as BRAVO board president until a year before
his death in 2015.

A few years after Charlie's passing, and in light of the fact that BRAVO was
then duplicating the efforts of its sister organization
[BUGATA](https://www.bu-gata.org/), the board decided to change the name and
focus of the organization to broader issues effecting the Latinx community in
Northern Virgina, and BRAVO became NOVALACIRO.

NOVALACIRO's first campaign was an effort to make it possible for residents
of Virginia to obtain drivers licenses regardless of their immigration status. 
After several years of struggle, this campaign, led by
[New Virginia Majority](https://www.newvirginiamajority.org/), eventually won,
when [Governor Northan signed the bill authorizing Virginia to issue drivers
licenses to undocumented immigrants living in the
state](https://wset.com/news/at-the-capitol/drivers-licenses).

For the last three years, NOVALACIRO has been focused on helping create
worker cooperatives in the community it serves. During October 2021,
Mujeres Manos a la Obra Virginia Cleaning will launch as a worker owned,
democratically run business.


## Links

* [Charles Rinker, leader in battle for affordable housing in
  Arlington, dies](https://www.insidenova.com/news/arlington/charles-rinker-leader-in-battle-for-affordable-housing-in-arlington-dies/article_3ea6e0ba-9a68-11e4-82fa-5f4088f767ba.html)
* [Charles Rinker: Speaking Out](https://library.arlingtonva.us/2020/11/12/charles-rinker-speaking-out/)
* [Progressive Voice: Charlie Rinker’s Legacy](https://www.arlnow.com/2015/01/29/progressive-voice-charlie-rinkers-legacy/)
