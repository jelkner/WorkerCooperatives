# Social Housing

Housing, along with other *physiological needs* like food and clothing, form
the bottom of the pyramid in
[Maslow's hierarchy of needs](https://en.wikipedia.org/wiki/Maslow%27s_hierarchy_of_needs).

In a just and equitable society, housing must be a right of all human beings,
not a [commidity](https://en.wikipedia.org/wiki/Commodity), and the
*de-commodification* housing should be a central demand of anyone calling
themself a socialist.

## Links

* [Vienna’s Unique Social Housing Program](https://www.huduser.gov/portal/pdredge/pdr_edge_featd_article_011314.html)
* [Vienna’s Affordable Housing Paradise](https://www.huffpost.com/entry/vienna-affordable-housing-paradise_n_5b4e0b12e4b0b15aba88c7b0)
* [How European-Style Public Housing Could Help Solve The Affordability Crisis](https://www.npr.org/local/305/2020/02/25/809315455/how-european-style-public-housing-could-help-solve-the-affordability-crisis)
* [Red Vienna: How Austria’s capital earned its place in housing history](https://citymonitor.ai/housing/residential-construction/red-vienna-how-austrias-capital-earned-its-place-in-housing-history)
* [How Vienna Cracked the Case of Housing Affordability](https://thetyee.ca/Solutions/2018/06/06/Vienna-Housing-Affordability-Case-Cracked/)
