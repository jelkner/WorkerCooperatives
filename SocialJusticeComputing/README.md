# Social Justice Computing

[Social Justice Computing](https://cis.scc.virginia.gov/EntitySearch/BusinessInformation?businessId=11676889)
was registered with the state of Virginia on March 29, 2024 by Ved Chudiwale,
Vrishin Hazari, Dr. Stephen Hubbard, and Jeffrey Elkner with the goal
of creating a non-profit orginization that could help fund projects using
computer programming in the service of social justice framed by the
[United Nations Sustainable Development Goals](https://sdgs.un.org/goals).

While registering with the
[Virginia State Corporation Commission](https://www.scc.virginia.gov/) (SCC)
and getting an
[Employer Identification
Number](https://en.wikipedia.org/wiki/Employer_Identification_Number) (EIN)
from the IRS where easy processes, in order to open a bank account we needed
to present the credit union with our
[bylaws](https://en.wikipedia.org/wiki/By-law), the creation of which is a
process with which none of us were familiar.


## Resource Links

* [Writing Your Nonprofit Bylaws + Template &
   Sample](https://nonprofitmavericks.com/start/non-profit-bylaws/)
