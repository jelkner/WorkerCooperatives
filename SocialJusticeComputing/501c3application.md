# Part III - Your Specific Activities

1. Briefly describe the organization's mission or most significant activities
   (limit 250 characters).

   > Use information technology in the service of social justice. To make
   > these goals clear and to connect us with the global movement toward their
   > attainment, we are guided by the 17 United Nations Sustainable Development
   > Goals.

Please review your activity narrative to ensure you fully describe the
most significant activity or activities you conduct or will conduct to
accomplish your tax-exempt 501(c)(3) purposes. Don't refer to or repeat
purposes in your organizing document or speculate about potential future
programs. You should describe either actual or planned mission or activities.

2. Enter the appropriate 3-character NTEE Code that best describes your
   activities (See the instructions).

   > S20

3. Date incorporated if a corporation, or formed if other than a corporation
   (MMDDYYY).

   > 03292024

4. State of Incorporation or other formation.

   > Virginia

5. Section of 501(c)(3) requires your organizing document must limit your
   purposes to one or more exempt purposes within section 501(c)(3).

   ✓ Check this box to attest that your organizing document contains this
   limitation.

6. Section of 501(c)(3) requires your organizing document must not empower
   you to engage, otherwise than as an insubstantial part of your activities,
   in activities that in themselves are not in furtherance of one or more
   exempt purposes.

   ✓ Check this box to attest that your organizing document does not expressly
   empower you to engage, otherwise than as an insubstantial part of your
   activities, in activities that in themselves are not in furtherance of one
   or more exempt purposes. 

7. Section of 501(c)(3) requires your organizing document must provide that
   upon dissolution, your remaining assets be used exclusively for section
   501(c)(3) exempt purposes. Depending on your entity type and the state in
   which you are formed, this requirement may be satisfied by operation of
   state law.

   ✓ Check this box to attest that your organizing document contains the
   dissolution provision required under section 501(c)(3) or that you do not
   need an express dissolution provision in your organizing document because
   you rely on the operation of state law in the state in which you are formed
   for your dissolution provision. 
