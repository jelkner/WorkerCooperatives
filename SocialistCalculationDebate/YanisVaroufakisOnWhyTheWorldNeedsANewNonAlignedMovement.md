# Yanis Varoufakis on why the world needs a new Non-Aligned Movement

On Friday, January 27, 2023, DiEM25 co-founder and MERA25 leader Yanis
Varoufakis gave a speech at the Havana Congress on the New International
Economic Order, about the need for a new Non-Aligned Movement to "end the
legalised robbery of people and Earth fuelling climate catastrophe."

Available on [YouTube](https://www.youtube.com/watch?v=6MfuGSlDRsc&t=2s).

## From Transcript

5:19
[W]e call upon democrats across the world to join forces in a New Non-Aligned
Movement as a route to lasting peace and globally shared prosperity. And there
you have it: Fifty years after the original Non-Aligned Movement, we're back at
square one. We're trying to build it up again. First in Athens, today in
Havana. Thankfully, we don't need to write new inspiring speeches.  We have
them, we have a good stock of them. All we need to do is to repeat the words of
Fidel at the United Nations Assembly in October of 1979, when he said:
5:58
"The din of weapons, threatening language, and arrogance on the international
scene must cease. Bombs can kill the starving, the diseased and ignorant, but
they cannot kill hunger, diseases or ignorance." And that "the international
monetary system," said Fidel, "predominating today is bankrupt and it must be
replaced." Well, nothing can be more current today.
6:48
So as we are restarting the process of building a New Non-Aligned Movement to
forge a New International Economic Order, we must ask ourselves: Why did we
lose the last time? Why were we so comprehensively defeated in the 1980s and
the 1990s?  Why did the original Non-Aligned Movement fall prey to
Neoimperialism's highest form, which is, of course, Globalisation,
Financialised Capitalist Globalisation?
7:23
The short answer is because capitalists, in practice, proved better
internationalists than we were. Because they understood the nature of
Neoimperialism better than we did and that's why they won.
7:45
What did they understand better than we did? They understood better than we
did the new audacious imperialism that was born in 1971, when Bretton Woods
collapsed and the United States dollar was no longer convertible to gold,
prompting Richard Nixon to send a message to Europeans, European governments
and the world's capitalists, saying: The dollar, as of today, is your problem.
And how right Nixon was.
8:16
As the American - the US, I shouldn't say American - as the US deficit
skyrocketed, the world was flooded with American dollars and the banks, the
central banks outside the United States were forced to use these US dollars
since they could not be converted to gold any more as the reserves with which
they backed their own currency. The dollar suddenly became something like an
IOU issued by the hegemon.  Before long, the global financial system was backed
by IOUs issued by a hegemon, who decided what foreigners holding those IOUs
could do or couldn't do with the IOUs issued by the hegemon.
9:06
America was now a fully fledged deficit country with a big trade deficit, but
it was nothing like any other deficit country in the world. You see, Argentina,
France, India, Greece needed to borrow dollars. America didn't need to borrow
dollars to back up its currency.
9:30
It didn't need to raise interests rates in order to prevent an exodus of
dollars, the exodus of dollars was the foundation of American hegemony. 
Capitalists in surplus countries, countries like Japan, Germany, and later of
course China, saw the American trade deficit as a great saviour.  It was a huge
vacuum cleaner. The American trade deficit that was sucking into America the
net exports of Germany, Japan, China.
10:06
And what did the Japanese, German, and later Chinese capitalists do with all
these dollars that they earned?  They sent them back to the United States -
couldn't do anything else with them - to buy property in the United States,
American government bonds, and a few companies that the American government
allowed them to buy - not Boeing, not Microsoft, none of the crucial ones.
10:35
Meanwhile, the deficit countries in the Global South, in Asia, in Latin
America, they constantly agonised over a shortage of dollars, which they had to
borrow from Wall Street to import medicines, energy, and the raw materials
necessary to produce their own exports for earning the dollars with which to
repay Wall Street.
10:55
Inevitably, every now and then, as you all know, the Global South deficit
nations ran out of dollars and could not repay Wall Street. That is when the
West sent in the bailiffs, the International Monetary Fund, that lent the
dollars on condition that the debtor government handed over the country's land,
water, ports, airports, electricity, telephone networks, even its schools and
hospitals to the local and to the international oligarchs who grabbed this
treasure, took rents, and what did they do with the rents?  Sent them to
American rentier capitalism, to invest them.
11:40
Washington, comrades, had found the magic formula that no other empire had
discovered before, of how to make wealthy foreigners and wealthy governments
and poor governments and the poor of the world finance the American government
and the net imports of the American economy.
12:03
A Chinese official once described to me globalisation as something that was
founded on a Dark Deal. That's how the Chinese official put it to me: a Dark
Deal. Why did he call it dark? Because it was founded on a dark, unspoken,
implicit pact between America's ruling class and foreign capitalists and
rentiers.
12:30
Let me put it slightly differently: Suppose you could end American hegemony
today, there is a button here, you can press it and end US hegemony. Who would
stop you form pressing it?  OK, the US authorities, the military, the CIA, Wall
Street, Silicon Valley, would try to stop you pressing this button, but they
are not alone! A crowd of non-Americans would stop you from pressing it,
including German industrialists, Saudi sheiks, Greek oligarchs, European
bankers and, yes, Chinese capitalists.
13:08
In other words, the supremacy of the dollar has been just as functional to the
interests of US rentier capitalism as it was to German, Argentinian, Nigerian,
Korean and Chinese capitalists.  Without the dollar's and America's global
dominance, Chinese, Japanese, Korean or German capitalists would not have been
able continually to extract colossal surplus value from their workers and then
stash it away in America's rentier economy.
13:44
Meanwhile, Argentinian, Greek, Russian, Ukrainian and Indian oligarchs would
not be able to loot our countries, take their public assets, liquidate them and
turn them into property rights in the United States.  The lesson for us is
simple: We must not repeat the mistake of thinking that the New International
Economic Order will be built because the governments or the elites of the South
are going to band together against the North, against Washington or the
European Union.
14:19
Our Non-Aligned Movement will fail if we give it a narrow role, a narrow role
of bringing together the G77, the BRICS, in opposition to the West.  We need to
beware not only the functionaries in London, in Paris, in Brussels, in
Washington, who work tirelessly to make sure that we fail, that nothing
changes, but also government officials who are very close to the interests of
capitalists in the Global South, including in China, and who use the US trade
deficit to exploit their people, their country, and then stash their dollarised
surplus in the United States, in the Cayman Islands, in Delaware, in the dollar
circuits.
15:09
Do we want to be true internationalists? If we do, then let's not forget who
are the one people, one people who have probably most to gain from the end of
US dominance: The American working class in the United States, who have been
condemned for decades to immiseration, to ‘deaths of despair’, to being called
deplorables. Yes, let us never forget that imperialism's victims have always
resided both in the Metropolis and in the Periphery. That the current
international economic order, which we want to smash, inflicts different types
of misery on workers everywhere.
15:57
Globalisation forced American workers into an immiseration brought on by
underinvestment and de-industrialisation.  It was as if part of the Global
South had moved into the Midwest, into the North of England, into parts of
Germany, into parts of Greece, of Italy, of Spain. At the same time, the same
globalisation process forced Chinese workers in the coastal towns of China to
suffer the frenzied exploitation associated with over-investment. It was as if
parts of the Global North had moved to these Chinese cities, but the people
working there were working with wages and conditions of the immiserised Global
South.  Different miseries, same recycling process, same recycling mechanism
that takes values extracted at the local level and recycles them globally.
Today, this same globalisation, which turned on American deficits, feeding
Chinese capital into American rents, this globalisation today is being
replaced, as we speak, by a New Cold War between the United States and China,
one that poses an imminent threat upon life on Earth.
17:25
What exactly is behind that new Cold War? China's frenzied industrialisation
was not a problem for Washington as long as the Dark Deal was operational, as
long as Chinese capitalists needed the dollar to turn the United States trade
deficit into an instrument for extracting surplus value from Chinese workers
and then depositing it in the United States. As long as this was going on,
Washington did not have a problem with the industrialisation of China. So what
has changed? Two things.  The first thing that has changed is the emergence of
a new type of capital. I call it cloud capital. Now, capital as Karl Marx
defined it so beautifully, is a produced means of production, like a steam
engine, like a tractor, a 3D printer today, or an industrial robot produced to
produce other things: a produced means of production.
18:26
What's cloud capital? Well, it is Internet-based, it lives on the cloud and it
is a produced means of behaviour modification. Behaviour modification is
something we always try to do. Every priest, orator, politician, author has
tried to modify the behaviour of people. Advertisers, that's what they do, they
try to modify our behaviour.  But what has changed is that today machines using
artificial intelligence are modifying behaviour, they're a capital good. Who
controls today cloud capital, controls, increasingly, the world's surplus
value. Now, how does this connect at all to the new Cold War?
19:21
Well, it is the reason Donald Trump started it. Remember when he banned Huawei
from using Android and then tried to americanise TikTok? Why did he do that?
America dominates the world by dominating finance through Wall Street and the
dollar, but it is also increasingly dominating the world through cloud capital:
Silicon Valley. China is threatening, has been threatening for a few years,
America's dominance in both fields: money and cloud capital.
20:02
Friends today, today as we speak, there is an application, an app, on Chinese
phones. Actually, not only Chinese phones, you can download it, it's called
WeChat. It belongs to Tencent, the Chinese conglomerate. Only today, 38 billion
messages will be exchanged on WeChat, many of which include payments. Anyone
who has that application can stream music, watch videos, read the news, and at
the same time, without exiting the app, make payments.
20:40
And that includes millions of non-Chinese who are connected to the Belt and
Road Initiative, who have connections with yuan-based bank accounts.  Now
consider the other development of great significance: In October of 2020, the
People's Bank of China initiated a remarkable experiment with a digital
currency. If you put together WeChat, or apps like WeChat from the Chinese
private sector, with the Central Bank of China's digital currency, what you
have is the only superhighway of money and data that can compete with that of
Wall Street, the Federal Reserve - central bank of the US - and Silicon Valley
put together. Until recently, that was not a problem for Washington, because
nobody really used that super highway, it was like building a huge road and
nobody's travelling on it.
21:41
Until Putin invaded Ukraine and in retaliation the United States confiscated
300 billion dollars of Russia's Central Bank money. Suddenly, non-American
money panicked - not just Russian money - and increasingly they have been using
this Chinese-built super highway. That is a worry for Washington and it is the
reason why Joe Biden last October launched total economic warfare against China
by declaring a microchip ban. Nothing to do with the military, nothing to do
with a worry that the Chinese military sector is going to improve rapidly and
overpower the Americans. It's got to do with this superhighway of money and
data.
22:34
Friends, comrades, we've never been closer to a nuclear holocaust than today.
When I was writing this speech I checked the Doomsday Clock. Remember the
Doomsday Clock from the 1940s? It showed 100 seconds before midnight. Today, I
checked it again, it's 90 seconds, apparently because of some German tanks
being sent to Ukraine. And that's without even looking at the other Doomsday
Clock which is ticking viciously, the one that is counting down the moment when
humanity is going to go past the point of no return from climate disaster.  And
what is the global ruling class doing to avert these catastrophes?  Nothing!
They are doing everything they can to push humanity over both cliffs at once.
23:21
This is why we need a New Non-Aligned Movement and a New International Economic
Order. At this point I think it is important, it helps to clarify what it means
politically, ideologically and ethically to be non-aligned.  It does not mean
to be neutral. As we stated in the Athens Declaration: We are steadfastly on
the side of the invaded, the victims of aggression, whether they are in
Palestine, in Yemen, in Cuba, in Western Sahara, or indeed in Ukraine.  But at
the same time, we must be the first to criticise any abuse of freedom and
democratic rights wherever it happens. Our New Non-Aligned Movement must
reclaim freedom from the liberals that have usurped it.  From Peru today, where
our comrades are being shot dead, to Iran, where heroic women are leading brave
men in their demonstrations against the theocratic regime,
under the banner WOMAN - LIFE - FREEDOM. You may object and ask: But isn't the
Iranian government not fighting against US imperialism?
Absolutely! However, just because a regime is at loggerheads with the United
States, with US imperialism, does not give them a free pass to violate the
basic freedoms and rights of our comrades in their countries.
25:00
You see what I'm getting at. Our truly Non-Aligned Movement must be
dialectical.  We must be able to defend the state of Iran from the
encroachments of the CIA, the Pentagon, US imperialism, while at once
supporting the progressives in Iran that are resisting the regime's corrupt
theocracy and the local agents of the CIA in Iran trying to subvert the
government of Iran.
25:33
You may well ask: And what about China? How should the New Non-Aligned Movement
approach the China question? In two ways, I suggest.  First, respectfully, in
view of the magnificent economic miracle performed by the people of China,
whose achievements we must defend against the encroachments of US imperialism
and the new Cold War.  And secondly, critically. Critically, not only in
relation to basic freedoms and human rights, but also in terms of good old
fashioned class struggle.  President Xi recently, over the last year or so, has
made some interesting pronouncements. Interesting promises of a clash with
China's rentiers, with China's big tech cloud capital owners, Jack Ma, for
instance, and with traditional capitalists. Xi has outlined a programme for
boosting workers' incomes, something that I believe he understands has to take
place against the model of mercantilism of basing Chinese growth on net
exports. Now this is a difficult task for the Chinese government and it would
be good to be achieved. Good for the Chinese people, good for a New
International Economic Order.
27:06
As it would be good for China, during this new exploding debt crisis in the
developing countries, in the Global South, to give massive, large haircuts to
the debt of countries that cannot repay. Those would be good steps in the
direction of a New International Economic Order and our New Non-Aligned
Movement should be pushing the Chinese leadership in that direction critically
and respectfully.
27:35
Now, turning to this concept of a New International Economic Order. How do we
envisage it? Any transition to a sustainable economic order will require a
large Green Investment Fund. Cuba's Foreign Minister Bruno Rodríguez Parrilla
in a recent G77 and China meeting put the necessary figure to between 3.3 and
4.5 trillion dollars a year.  I say that nothing short of 10 trillion dollars a
year will do. Are these feasible numbers?  Yes, they are technically, of course
they are. Imagine a repurposed World Bank, backed by a digital world currency -
currency unit, like the SDRs - issued by a repurposed International Monetary
Fund, where the deal is, internationally, that all capital and trade flows are
denominated in this currency unit and 10% of those flows are converted into
green investments especially in the Global South. Technically it is
straightforward, however, politically it's not. Green Keynesianism will fail
for reasons that Michał Kalecki, for those of you who remember Michał Kalecki
from the 1930s and 40s, - Polish marxist economist - explained so vividly when
he was criticising Keynesianism.  What he said was this: Even if the
bourgeoisie panics and adopts Keynesian policies to save its own skin, the
moment those policies begin to work, the first four or five minutes, the
bourgeoisie will ditch these policies and will go back to its own extractive,
exploitative policies, because - let's face it - it is in the capitalist class'
nature to block the very road that leads to its own salvation.
29:37
Which means only one thing. That to implement the necessary Green Keynesian
policies, we must do something that Keynes would have hated: Dismantle the
Global Empire of Capital, which prefers to see the end of the world, than the
implementation of the Green Keynesian policies that could prevent the end of
the world. Now, how do we do that?  Well, nothing short of a revolution will do
comrades! It's really very simple. Now, that may sound like an impossible
dream.  Well, it is a prerequisite for saving humanity, exactly the same way
that we were taught by Fidel and by all those leaders of the progressive
movements of the past.
30:27
How do we do it? Our revolution must use the technology of Big Tech. Their
cloud capital, in order to cooperate and in order to activate our campaigns
across the board internationally, in a coordinated and effective fashion. A
first step, the Progressive International has shown the light to: Every year,
every December, the Progressive International organises a strike against
Amazon: Make Amazon Pay (makeamazonpay.com) And how should that be? Well, to
reclaim its plundered Commons: our lands, our oceans, the air,
very soon outer space, they are privatizing it. I don't know whether you know
that, they are doing it already.
31:20
There are two things we need to do internationally. We need to legislate so
that corporations belong to those who work in them on the basis of one person -
one share - one vote and we need to deny banks the monopoly on payments.  31:36
Banks and profit will then wither as our economies wean themselves off share
markets and the financial sector.
31:48
At the same time, profit and wages are no longer meaningful concepts, since
everybody will be a shareholder in the company in which they work.  This
simultaneous death of the market for shares and of the labour market, along
with the defanging of banks, will automatically redistribute wealth,
make it possible to offer a basic income to everyone and, as a magnificent
byproduct, remove the major incentives to war.
32:22
The end of capital's power over society will allow communities collectively to
decide health provision, education and investment, investment in saving the
environment from our virus-like growth. Genuine democracy would only then
become possible, to be practised in the workers assemblies and in the citizens
assemblies, not behind closed doors where oligarchs meet.
32:48
This twin democratisation of money and capital sounds like an impossible dream,
doesn't it? But I submit to you, comrades, it's not more impossible or it
doesn't sound more impossible than once the idea of one person - one vote, of
the end of slavery, of the end of the divine right of Kings sounded.  This twin
democratisation is nothing short of a precondition for the survival of us as a
species, it is that simple. Those are the tasks of the New Non-Aligned Movement
we must now build. Its ultimate purpose: To end the legalised robbery of people
and planet which is fuelling climate catastrophe. Nothing less than the total
vanquishing of capital's authority over human societies can end depravity and save the planet.
33:48
Comrades, friends, this is not a drill and it is not a pipe dream.  We can pull
it off. Together! Thank you. [Applause]
