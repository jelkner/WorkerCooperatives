# Email to [All Things Co-op](https://www.democracyatwork.info/atc)

I sent the following email in response to the podcast episode,
[Accumulation - The Ruthless Pursuit of Profit](https://www.democracyatwork.info/atc_accumulation_ruthless_pursuit_of_profit),
on Sunday, January 23, 2022:

---

Dear Comrades,

I just finished listening to your episode, "Accumulation - The Ruthless
Pursuit of Profit", and wanted to share a reflection coming from being
a high school teacher who has spent years looking for a way to avoid
playing what I call the "grade game" with my students, so that we can
better focus on learning.

The "grade game" is the focus, sometimes obsessive, that many students
have on accumlating "points" toward improving their "grades". The
evaluation system we generally use actively reinforces this behavior,
which is characterized by the question "What can I do to improve my
grade?" instead of "What can I do to learn?"

This strikes me as somewhat analogous to the use value vs. exchange
value contradiction within capitalism. By focusing on the exchange
value, grades, we distort and pervert what the educational system
should be providing.

The problem may also be related to its focus on quantitative rather
than qualitative measures of "success". When everything becomes a
number, that most abstract and fungible thing, we tend to focus on
counting and accumulating rather than evaluating, relating, and
interconnecting.

I have to give grades, but I use a system that I've developed over the
years that intentionally makes the evaluation system more
qualititative, and helps put the focus on learning rather than box
checking and accumlating points. Students seem to really like it, and
even those most wired for "accumulation", and desparate for ways to
earn points, adapt to my system and begin to focus more on learning
objectives instead.

I know Cuba did some experiments with moral vs. material incentives
under Che's leadership.  I would love to know more about what comrades
in Cuba who are now looking at incorporating worker coops into the
economic system think about the lessons learned then and how that will
impact what they try to do now.

Camila made important points about this topic when you had her on your
podcast.  Worker coops by themselves will not resolve the use value vs.
exchange value contradiction as long as their ultimate success is
measured by the M' - C - M'' relationship.

We tend to become what we measure.  It seems to me that we will need a
new socialist accounting system, which can somehow encourage workers in
our democratic work places to focus on producing things we need instead
of things that will sell, and will help dampen rather than excite our
tendencies toward blind accumulation.

Yours for democracy at work,
Jeff Elkner
